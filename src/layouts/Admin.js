import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { Container } from "reactstrap";
import { connect } from "react-redux";

import AdminNavbar from "../components/base/Navbars/AdminNavbar.js";
import AdminFooter from "../components/base/Footers/AdminFooter.js";
import Sidebar from "../components/base/Sidebar/Sidebar.js";
import Header from "../components/base/Headers/Header";

import {
  PostList,
  Dashboard,
  FeatureList,
  OrderList,
  PackageList,
  TemplateList,
  OrderDetail,
  UserList,
  UserCreate,
  UserDetail,
  PartnerList,
  PartnerCreate,
  PartnerDetail,
} from "../pages";
import routes from "../pages/routes";
import { getProfile } from "../store/actions/userAction.js";

class Admin extends React.Component {
  componentDidUpdate(e) {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.mainContent.scrollTop = 0;
  }
  getBrandText = (path) => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  render() {
    return (
      <div>
        <Sidebar
          {...this.props}
          routes={routes}
          logo={{
            innerLink: "/admin/dashboard",
            imgSrc: require("../assets/img/brand/argon-react.png"),
            imgAlt: "...",
          }}
        />
        <div className="main-content" ref="mainContent">
          <AdminNavbar
            {...this.props}
            // brandText={this.getBrandText(this.props.location.pathname)}
          />
          <Header />
          <Switch>
            <Route exact path="/admin/dashboard" component={Dashboard} />
            <Route exact path="/admin/feature" component={FeatureList} />
            <Route exact path="/admin/package" component={PackageList} />
            <Route exact path="/admin/order" component={OrderList} />
            <Route exact path="/admin/order/:orderId" component={OrderDetail} />
            <Route exact path="/admin/template" component={TemplateList} />
            <Route exact path="/admin/user" component={UserList} />
            <Route exact path="/admin/user/create" component={UserCreate} />
            <Route exact path="/admin/user/:userId" component={UserDetail} />
            <Route exact path="/admin/partner" component={PartnerList} />
            <Route exact path="/admin/partner/create" component={PartnerCreate}/>
            <Route exact path="/admin/partner/:partnerId" component={PartnerDetail}/>
            <Route exact path="/admin/post" component={PostList} />
            <Redirect from="*" to="/admin/dashboard" />
          </Switch>
          <Container fluid>
            <AdminFooter />
          </Container>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => {
  return {
    getProfile: dispatch(getProfile()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
